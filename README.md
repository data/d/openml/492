# OpenML dataset: newton_hema

https://www.openml.org/d/492

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Michael Newton (newton@stat.wisc.edu)  
**Source**: [StatLib](http://lib.stat.cmu.edu/datasets/) - 11-6-93  
**Please cite**:   

Data on fluctuating proportions of marked cells in marrow from heterozygous Safari cats from a study of early hematopoiesis.

The data included below are 11 time series of proportions of marked progenitor cells from the bone marrow of the hybrid Safari
cat.  These data come from experiments done by J. L. Abkowitz and colleagues at the University of Washington, Seattle.  

There are four columns and a total of 140 records. The first column is an id for the cat in the study. The second colum records the
time, in weeks from the start of monitoring, that the measurement from marrow is recorded. The third column gives the percent of domestic-type progenitor cells observed in a sample of cells at that time. The fourth column gives the sample size at that time, i.e. the number of progenitor cells analyzed.

For background on the data, see:  
Abkowitz et al., 1988, Blood 71:1687--1692  
Abkowitz et al., 1990, PNAS, 87:9062--9066  
Abkowitz et al, 1993, Blood, 82:2096--2103  
Guttorp et al., 1990, IMA J. Math. App. Med. Bio., 7:125--143  

These particular data are used in an analysis by Newton et al, 1993 ``Stochastic Modeling of Early Hematopoiesis.''

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/492) of an [OpenML dataset](https://www.openml.org/d/492). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/492/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/492/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/492/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

